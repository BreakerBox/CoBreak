require 'openssl'
require 'sequel'
require 'cobreak/version'
class Decrypt
  def show(mode, dato)
    decrypt = OpenStruct.new
    decrypt.mode = mode.downcase
    decrypt.wordlist = File.join(Gem.path[1], "gems", "cobreak-#{CoBreak.version}", 'lib', 'cobreak', 'show', "#{decrypt.mode}.db")
    dbs = Sequel.sqlite
    dbs.create_table? :hashes do
      String :original
      String :hash
    end
      if (decrypt.mode.eql?('md4'))
        decrypt.hash = OpenSSL::Digest::MD4.new
      elsif (decrypt.mode.eql?('md5'))
        decrypt.hash = OpenSSL::Digest::MD5.new
      elsif (decrypt.mode.eql?('sha1'))
        decrypt.hash = OpenSSL::Digest::SHA1.new
      elsif (decrypt.mode.eql?('sha224'))
        decrypt.hash = OpenSSL::Digest::SHA224.new
      elsif (decrypt.mode.eql?('sha256'))
        decrypt.hash = OpenSSL::Digest::SHA256.new
      elsif (decrypt.mode.eql?('sha384'))
        decrypt.hash = OpenSSL::Digest::SHA384.new
      elsif (decrypt.mode.eql?('sha512'))
        decrypt.hash = OpenSSL::Digest::SHA512.new
      elsif (decrypt.mode.eql?('ripemd160'))
        decrypt.hash = OpenSSL::Digest::RIPEMD160.new
      end
    File.foreach(decrypt.wordlist) {|line|
      line.chomp!
      dbs[:hashes] << {original:line, hash:decrypt.hash.hexdigest(line)}
    }
   decrypt.pass = dbs[:hashes].filter(hash:dato).map(:original)
   unless (decrypt.pass.empty?)
     puts "\e[1;32m[\e[0m+\e[1;32m]\e[0m Hash Found: #{decrypt.pass.join(',')}"
   else
     puts "\e[1;31m[\e[0m+\e[1;31m]\e[0m Hash Not Found in Database..."
   end
  end
end
DeCrypt = Decrypt.new
