require 'ostruct'
module CoBreak
  class BruteChars
    def initialize(options)
      @options = options
      @hash = %w[MD4 MD5 SHA1 SHA224 SHA256 SHA384 SHA512 RIPEMD160]
    end
    def banner_chars()
      puts "\e[32m╭─[\e[0m CoBreak: #{CoBreak.version}"
      unless (@options.range.nil?)
        puts "\e[32m├─[\e[0m Range: #{@options.range[0]} #{@options.range[1]}"
      else
        puts "\e[31m├─[\e[0m Range Not Found"
      end
      unless (@options.chars.nil?) or (@options.chars.empty?)
        puts "\e[32m├─[\e[0m Characters: #{@options.chars}"
      else
        puts "\e[31m├─[\e[0m Characters Not Found"
      end
      if (@hash.include?(@options.bruteforce.to_s.upcase))
        puts "\e[32m├─[\e[0m Type Hash: #{@options.bruteforce.upcase}"
      else
        puts "\e[31m├─[\e[0m Type Hash Not Found"
      end
      unless (@options.algo.nil?) or (@options.algo.empty?)
        puts "\e[32m╰─[\e[0m Hash: #{@options.algo}\n\n"
      else
        puts "\e[31m╰─[\e[0m Hash Not Found"
      end
    end
    def chars()
#      if (@options.range.empty?) or (@options.chars.nil?) or (@param.algo.nil?)
#        abort "\n"
#      end"
      if (@hash.include?(@options.bruteforce.upcase))
        if (File.exists?(@options.algo.to_s))
          IO.foreach(@options.algo){|line|
            line.chomp!
            if (@options.bruteforce.to_s.downcase.eql?('md4'))
              ForzeBrute::md4(line, @options)
            elsif (@options.bruteforce.to_s.downcase.eql?('md5'))
              ForzeBrute::md5(line, @options)
            elsif (@options.bruteforce.to_s.downcase.eql?('sha1'))
              ForzeBrute::sha1(line, @options)
            elsif (@options.bruteforce.to_s.downcase.eql?('sha224'))
              ForzeBrute::sha224(line, @options)
            elsif (@options.bruteforce.to_s.downcase.eql?('sha256'))
              ForzeBrute::sha256(line, @options)
            elsif (@options.bruteforce.to_s.downcase.eql?('sha384'))
              ForzeBrute::sha384(line, @options)
            elsif (@options.bruteforce.to_s.downcase.eql?('sha512'))
              ForzeBrute::sha512(line, @options)
            elsif (@options.bruteforce.to_s.downcase.eql?('ripemd160'))
              ForzeBrute::ripemd160(line, @options)
            end
          }
        else
          if (@hash.include?(@options.bruteforce.upcase))
            ForzeBrute::chars(@options.algo, @options.range, @options.chars, @options.bruteforce)
          end
        end
      end
    end
  end
end
