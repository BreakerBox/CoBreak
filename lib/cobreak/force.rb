require 'cobreak/function_hash'
class Forze_brute
  def initialize(author = 'BreakerBox')
    @author = author
  end
  def verify(dato, word = File.join(Gem.path[1], "gems", "cobreak-#{CoBreak.version}", 'lib', 'cobreak', 'hash', 'hash.db'))
    hash_db = Sequel.sqlite
    hash_db.create_table? :datahash do
      String :ori
      String :has
    end
    IO.foreach(word) {|lin|
      lin = lin.chomp
      hash_db[:datahash] << {ori:lin, has:dato}
    }
    ha = hash_db[:datahash].filter(ori:dato).map(:has)
    arr = Array.new
    arr << dato
    if (ha == arr)
      puts "\e[1;32m[\e[0m+\e[1;32m]\e[0m Hash already existing in the database: #{dato}"
      puts "\e[1;32m[\e[0m+\e[1;32m]\e[0m show the hash using --show, see the help parameter for more information"
      exit
    end
  end
  def word(dato, wordlist, type)
    verify(dato)
    forzebrute = OpenStruct.new
    forzebrute.hash = dato
    forzebrute.type = type
    forzebrute.wordlist = wordlist
      if (forzebrute.type.downcase.eql?('md4'))
        forzebrute.crypt = OpenSSL::Digest::MD4.new
      elsif (forzebrute.type.downcase.eql?('md5'))
        forzebrute.crypt = OpenSSL::Digest::MD5.new
      elsif (forzebrute.type.downcase.eql?('sha1'))
        forzebrute.crypt = OpenSSL::Digest::SHA1.new
      elsif (forzebrute.type.downcase.eql?('sha224'))
        forzebrute.crypt = OpenSSL::Digest::SHA224.new
      elsif (forzebrute.type.downcase.eql?('sha256'))
        forzebrute.crypt = OpenSSL::Digest::SHA256.new
      elsif (forzebrute.type.downcase.eql?('sha384'))
        forzebrute.crypt = OpenSSL::Digest::SHA384.new
      elsif (forzebrute.type.downcase.eql?('sha512'))
        forzebrute.crypt = OpenSSL::Digest::SHA512.new
      elsif (forzebrute.type.downcase.eql?('ripemd160'))
        forzebrute.crypt = OpenSSL::Digest::RIPEMD160.new
      end
      forzebrute.time = Time.now
      File.foreach(forzebrute.wordlist) {|line|
        line.chomp!
        if (forzebrute.crypt.hexdigest(line).eql?(forzebrute.hash))
          puts "\e[1;32m[\e[0m+\e[1;32m]\e[0m Decrypted Text: " + line
          puts "\e[1;32m[\e[0m+\e[1;32m]\e[0m Hash Cracking #{Time.now - forzebrute.time} seconds"
          $datBas::database(forzebrute.crypt.hexdigest(line))
          DB::database(line, File.join(Gem.path[1], "gems", "cobreak-#{CoBreak.version}", 'lib', 'cobreak', 'show', "#{forzebrute.type}.db"))
          exit
        end
      }
   if true
     puts "\e[1;31m[\e[0m+\e[1;31m]\e[0m Not Decrypted Text: #{dato}"
     puts "\e[1;31m[\e[0m+\e[1;31m]\e[0m Time: #{Time.now - forzebrute.time} seconds"
   end
  end
  def chars(dato, range, char, type)
    verify(dato)
    forzechars = OpenStruct.new
    forzechars.dato = dato
    forzechars.range = range
    forzechars.char = char.chars
    forzechars.type = type
    if (forzechars.type.downcase.eql?('md4'))
      forzechars.crypt = OpenSSL::Digest::MD4.new
    elsif (forzechars.type.downcase.eql?('md5'))
      forzechars.crypt = OpenSSL::Digest::MD5.new
    elsif (forzechars.type.downcase.eql?('sha1'))
      forzechars.crypt = OpenSSL::Digest::SHA1.new
    elsif (forzechars.type.downcase.eql?('sha224'))
      forzechars.crypt = OpenSSL::Digest::SHA224.new
    elsif (forzechars.type.downcase.eql?('sha256'))
      forzechars.crypt = OpenSSL::Digest::SHA256.new
    elsif (forzechars.type.downcase.eql?('sha384'))
      forzechars.crypt = OpenSSL::Digest::SHA384.new
    elsif (forzechars.type.downcase.eql?('sha512'))
      forzechars.crypt = OpenSSL::Digest::SHA512.new
    elsif (forzechars.type.downcase.eql?('ripemd160'))
      forzechars.crypt = OpenSSL::Digest::RIPEMD160.new
    end
    forzechars.result = Array.new
    forzechars.time = Time.now
    for range in (forzechars.range[0].to_i..forzechars.range[1].to_i).to_a
      for chars in forzechars.char.repeated_permutation(range).map(&:join)
        if (forzechars.crypt.hexdigest(chars)) == (forzechars.dato)
        puts "\e[1;32m[\e[0m+\e[1;32m]\e[0m Decrypted Text: #{chars}"
        puts "\e[1;32m[\e[0m+\e[1;32m]\e[0m Hash Cracking #{Time.now - forzechars.time} seconds"
        $datBas::database(forzechars.crypt.hexdigest(chars))
        DB::database(chars, File.join(Gem.path[1], "gems", "cobreak-#{CoBreak.version}", 'lib', 'cobreak', 'show', "#{forzechars.type}.db"))
        exit
        end
      end
    end
    if true
      puts "\e[1;31m[\e[0m+\e[1;31m]\e[0m Not Decrypted Text..."
      puts "\e[1;31m[\e[0m+\e[1;31m]\e[0m Time: #{Time.now - forzechars.time}"
    end
  end
end
ForzeBrute = Forze_brute.new

