require 'openssl'
require 'cobreak/function_db' 
require 'cobreak/function_hash'
require 'cobreak/version'
class Encrypt
  def show(mode, dato)
    encrypt = OpenStruct.new
    encrypt.mode = mode.downcase
    encrypt.dato = dato
    if (encrypt.mode.eql?('md4'))
      encrypt.crypt = OpenSSL::Digest::MD4.hexdigest(dato)
    elsif (encrypt.mode.eql?('md5'))
      encrypt.crypt = OpenSSL::Digest::MD5.hexdigest(dato)
    elsif (encrypt.mode.eql?('sha1'))
      encrypt.crypt = OpenSSL::Digest::SHA1.hexdigest(dato)
    elsif (encrypt.mode.eql?('sha224'))
      encrypt.crypt = OpenSSL::Digest::SHA224.hexdigest(dato)
    elsif (encrypt.mode.eql?('sha256'))
      encrypt.crypt = OpenSSL::Digest::SHA256.hexdigest(dato)
    elsif (encrypt.mode.eql?('sha384'))
      encrypt.crypt = OpenSSL::Digest::SHA384.hexdigest(dato)
    elsif (encrypt.mode.eql?('sha512'))
      encrypt.crypt = OpenSSL::Digest::SHA512.hexdigest(dato)
    elsif (encrypt.mode.eql?('ripemd160'))
      encrypt.crypt = OpenSSL::Digest::RIPEMD160.hexdigest(dato)
    end
    unless (encrypt.crypt.nil?)
      puts "\n\e[1;32m[\e[0m+\e[1;32m]\e[0m Encrypted Text: #{encrypt.crypt}"
      $datBas::database(encrypt.crypt)
      DB::database(dato, File.join(Gem.path[1], "gems", "cobreak-#{CoBreak.version}", 'lib', 'cobreak', 'show', 'md4.db'))
    else
      puts "\n\e[1;31m[\e[0m+\e[1;31m]\e[0m Not Encrypt Text..."
    end
  end
end
EnCrypt = Encrypt.new
