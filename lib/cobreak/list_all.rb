module CoBreak
  class List
    #all list formats and types
    def initialize(options)
      type = Array.new
      type << "Base64" << "Base32" << "Base16" << "Ascii85" << "Binary" << "Cesar"
      if (options.list.eql?("type"))
        puts "\nMode Cipher:"
        puts type
      end
      format = Array.new
      format << "MD4" << "MD5" << "SHA1" << "SHA224" << "SHA256" << "SHA384" << "SHA512" << "RIPEMD160"
      if (options.list.eql?("format"))
        puts "\nMode Cryptography:"
        puts format
      end
    end
  end
end
