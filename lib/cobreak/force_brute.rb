#!/bin/env ruby
require 'cobreak/version'
module CoBreak
  class BruteForze
    def initialize(options)
      @options = options
      @hash = %w[MD4 MD5 SHA1 SHA224 SHA256 SHA384 SHA512 RIPEMD160]
    end
    begin
      require 'cobreak/force'
    rescue LoadError => e
      puts e.message
      abort "reinstall gem new"
    end
    def banner_wordlist()
      puts "\e[32m╭─[\e[0m CoBreak: #{CoBreak.version}"
      if (File.exists?(@options.wordlist.to_s))
        puts "\e[32m├─[\e[0m Wordlist: #{File.expand_path(@options.wordlist)}"
      else
        puts "\e[31m├─[\e[0m WordList Not Found"
      end
      if (@hash.include?(@options.bruteforce.to_s.upcase))
        puts "\e[32m├─[\e[0m Type Hash: #{@options.bruteforce.upcase}"
      else
        puts "\e[31m├─[\e[0m Type Hash Not Found"
      end
      unless (@options.algo.nil?) or (@options.algo.empty?)
        puts "\e[32m╰─[\e[0m Hash: #{@options.algo}\n\n"
      else
        puts "\e[31m╰─[\e[0m Hash Not Found"
      end
    end
    def wordlist()
      if (@options.wordlist.nil?) or (@options.wordlist.empty?) or ('-'.include?(@options.wordlist.to_s))
        abort "\n"
      end
      if (@hash.include?(@options.bruteforce.to_s.upcase))
        if (File.exists?(@options.algo.to_s))
          IO.foreach(@options.algo.to_s){|line|
            line.chomp!
            if (@options.bruteforce.to_s.downcase.eql?('md4'))
              ForzeBrute::word(line, @options.wordlist, @options.bruteforce.to_s)
            end
          }
        else
          if (@hash.include?(@options.bruteforce.upcase))
            ForzeBrute::word(@options.algo.to_s, @options.wordlist, @options.bruteforce.to_s)
          end
        end
      end
    end
  end
end
