require 'rake'
Gem::Specification.new do |info|
  info.name        = 'cobreak'
  info.version     = '1.0.2'
  info.executables << "cobreak"
  info.description = "The CoBreak script is an cipher and cryptography tool"
  info.add_development_dependency "bundler", "~> 1.5"
  info.add_development_dependency  "openssl", "~> 2.2.0"
  info.add_development_dependency  "sequel", "~> 5.44.0"
  info.add_development_dependency  "sqlite3", "~> 1.4.2"
  info.add_runtime_dependency "sequel", "~> 5.45.0", ">= 2.0.0"
  info.add_runtime_dependency "sqlite3", "~> 1.4.2", ">= 1.4.0"
  info.add_runtime_dependency "base16", "~> 0.0.2", ">= 0.0.1"
  info.add_runtime_dependency "base32", "~> 0.3.4", ">= 0.3.0"
  info.add_runtime_dependency "base64", "~> 0.1.0", ">= 0.0.1"
  info.add_runtime_dependency "Ascii85", "~> 1.1.0", ">= 1.0.0"
  info.add_runtime_dependency "openssl", "~> 2.2.0", ">= 2.0.0"
  info.authors     = ["BreakerBox"]
  info.email       = 'breakerhtb@gmail.com'
  info.summary     = "Force Brute, Cipher, Cryptography"
  info.files = Dir['{lib}/**/*.rb', '{bin}/*', 'lib/**/**/*.db', '*.txt', '*.md', 'Gemfile', 'LICENCE', '*.gemspec']
  info.homepage    = 'https://github.com/BreakerBox/CoBreak'
  info.license       = 'MIT'
  info.post_install_message = "thanks for installing my gem"
end
